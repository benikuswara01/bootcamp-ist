FROM node:alpine

WORKDIR /reactjs

# install app dependencies
COPY package.json /reactjs

RUN npm install --force

# add app
COPY . /reactjs
EXPOSE 3000

# start app
CMD ["npm", "start"]
