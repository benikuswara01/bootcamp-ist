import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {Paper, Table, TableBody, TableCell, TableContainer, TableHead, TablePagination, TableRow} from '@material-ui/core'


const columns = [
  { id: 'sender', label: 'Pengirim &\n No.Rekening', minWidth: 100, align: 'left', },
  { id: 'recipient', label: 'Penerima &\n No.Rekening', minWidth: 100, align: 'left', },
  // ISO\u00a0Code
  {
    id: 'nominal',
    label: 'Nominal',
    minWidth: 50,
    align: 'center',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'description',
    label: 'Keterangan',
    // Size\u00a0(km\u00b2)
    minWidth: 50,
    align: 'center',
    format: (value) => value.toLocaleString('en-US'),
  },
  {
    id: 'time',
    label: 'Waktu',
    minWidth: 120,
    align: 'center',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'status',
    label: 'Status',
    minWidth: 50,
    align: 'center',
    format: (value) => value.toFixed(2),
  },
  {
    id: 'transaction',
    label: 'Transaksi',
    minWidth: 110,
    align: 'center',
    format: (value) => value.toFixed(2),
  },
];

function createData(sender, recipient, nominal, description, time, status, transaction) {
  const density = nominal;
  return { sender, recipient, nominal, description, time, status, transaction, density };
}

const rows = [
  createData(`${'Nanda'} - ${78945612}`, `${'Handoko Kusuma'} - ${78945612}`, 90000, 'Keterangan', `${'15/10/2020 14:50'}`,`${'Income'}`, `${'ECZ-11478963'}` ),
  createData(`${'Keisha Salsa'} - ${78945612}`, `${'Handoko Kusuma'} - ${78945612}`, 90000, 'Keterangan', `${'15/10/2020 14:50'}`, `${'Outcome'}`,`${'ECZ-11478963'}`),
  createData('Italy', 'IT', 90000, 'Keterangan', `${'15/10/2020 14:50'}`),
  // createData('United States', 'US', 90.000, 'Keterangan', `${'15/10/2020 14:50'}`),
  // createData('Canada', 'CA', 90000, 'Keterangan', `${'15/10/2020 14:50'}`),
  // createData('Australia', 'AU', 90000, 'Keterangan', `${'15/10/2020 14:50'}`),
  // createData('Germany', 'DE', 90000, 'Keterangan', `${'15/10/2020 14:50'}`),
  // createData('Ireland', 'IE', 90000, 'Keterangan', `${'15/10/2020 14:50'}`),
  // createData('Mexico', 'MX', 90000, 'Keterangan', `${'15/10/2020 14:50'}`),
  // createData('Japan', 'JP', 90000, 'Keterangan', `${'15/10/2020 14:50'}`),
];

const useStyles = makeStyles({
  root: {
    width: '',
  },
  container: {
    maxHeight: 440,
  },
});

export default function StickyHeadTable() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(2);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className='w-3/5 h-max rounded-lg border-2 border-[#ECEEF7]' style={{boxShadow:'none'}}>
      <div className='pt-3 px-5 mb-3 font-bold text-xl'>
        Transaksi List
      </div>
      <TableContainer className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth, fontSize:10, backgroundColor:'white', color:'#949494'}}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((row) => {
              return (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align} style={{textAlign:'center', fontSize:10}}>
                        {column.format && typeof value === 'number' ? column.format(value) : value}
                      </TableCell>
                    );
                  })}
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
      {/* <TablePagination
        rowsPerPageOptions={[2, 10, 25, 100]}
        component="div"
        count={rows.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      /> */}
    </Paper>
  );
}
