import { Grid, Paper } from '@material-ui/core'
import { makeStyles } from '@material-ui/styles'
import React from 'react'

const useStyles = makeStyles((theme) => ({
   root: {
      flexGrow: 1,
   },
   paper: {
      padding: '20px',
      color: '#343434',
      fontWeight:700,
      border: '2px solid #ECEEF7',
      boxShadow:'none',
      borderRadius:8
   }
}))

const PaperGridComponent = (props) => {
   const classes = useStyles()
   
   return (
      <React.Fragment>
         <Grid item xs={props.xs} sm={props.sm}>
            <Paper className={classes.paper}>
               <div className='flex row gap-x-[13px] my-[9px] items-center'>
                  <div className='text-[15px]'>{props.title}</div>
                  <div className='flex row justify-between font-normal text-sm rounded-3xl px-1.5 py-0 w-max h-max bg-[#C10A0A26] text-[#C71026]'>
                     {props.icon} <span>{props.percentage}</span> 
                  </div>
               </div>
               <div>
                  <span className='text-2xl'>{props.total}</span> <br/>
                  <span className='text-[#949494] font-normal text-sm'>{props.desc}</span>
               </div>
               {props.children}
            </Paper>
         </Grid>
               
         {/* <div className='block'>
            <div className='flex row gap-x-[13px] my-[9px] items-center'>
              <div className='text-[15px]'>Total Transaksi</div>
              <div className='flex row justify-between font-normal text-sm rounded-3xl px-1.5 py-0 w-max h-max bg-[#23C10A26] text-[#0B8A00]'>
                <TrendingUp className='w-3'/> 9.0%
              </div>
            </div>
            <div>
              <span className='text-2xl'>3,342</span> <br/>
              <span className='text-[#949494] font-normal text-sm'>Transaksi</span>
            </div>
          </div> */}
      </React.Fragment>
  )
}

export default PaperGridComponent