import {Drawer, List, ListItem, ListItemIcon, ListItemText, makeStyles, Toolbar} from '@material-ui/core'
import logoImg from "../assets/logo.svg"
import logoText from "../assets/wallet-dough.svg"
import chart from '../assets/chart-square.svg'
import user from '../assets/user-square.svg'
import news from '../assets/newspaper.svg'
import React from 'react'
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
   drawer:{
      border:'none',
      outlineColor:'none',
      "& .MuiDrawer-paper": { 
         borderWidth: 0,  
         boxShadow: '0px 20px 50px rgba(220, 224, 249, 0.3)', 
      }
   },
   logoText:{
      marginTop:-14,
      width:72,
      marginLeft:-4
   },
   list:{
      width:273,
      marginTop:22,
      display:'flex',
      flexDirection:'column',
      rowGap:18,
   },
   listItem:{
      paddingLeft:45,
      display:'flex',
      flexDirection:'row',
      columnGap:18,
      alignSelf:'center',
   },
   listIcon:{
      minWidth:'0px',
   },
   toolbar:{
      display:'flex',
      flexDirection:'row',
      columnGap:8,
      alignItems:'center',
      justifyContent:'center',
      alignSelf:'center',
      marginBottom:16
   }
}))

const SidebarComponent = () => {
   const classes = useStyles()

   return (
      <React.Fragment>
         <Drawer variant='permanent' className={classes.drawer}>
            <List disablePadding className={classes.list}>   
               <Toolbar className={classes.toolbar}>
                  <img src={logoImg}alt="" width={80} />
                  <img src={logoText} alt="" className={classes.logoText} />
               </Toolbar>
               <ListItem button component={Link} to='/dashboard' className={classes.listItem}>
                  <ListItemIcon className={classes.listIcon}>
                     <img src={news} alt="" />
                  </ListItemIcon> 
                  <ListItemText primary='Dashboard'/>
               </ListItem>
               <ListItem button component={Link} to='/chart-transaction' className={classes.listItem}>
                  <ListItemIcon className={classes.listIcon}>
                     <img src={chart} alt="" />
                  </ListItemIcon>
                  <ListItemText primary='Chart Transaksi'/>
               </ListItem>
               <ListItem button component={Link} to='/user-accounts' className={classes.listItem}>
                  <ListItemIcon className={classes.listIcon}>
                     <img src={user} alt="" />
                  </ListItemIcon>
                  <ListItemText primary='Akun Pengguna'/>
               </ListItem>
            </List>
         </Drawer>
      </React.Fragment>
   )
}

export default SidebarComponent