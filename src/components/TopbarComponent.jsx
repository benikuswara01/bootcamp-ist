import { AppBar, IconButton, makeStyles, Toolbar, Typography } from '@material-ui/core'
import logoutIcon from '../assets/log-out.svg'
import photoIcon from "../assets/photo.svg"
import React from 'react'

const useStyles = makeStyles((theme) => ({
  toolbar:{
    display:'flex',
    flexDirection:'row',
    justifyContent:'space-between'
  },
  typography: {
    color:'#202327',
    fontSize:20,
    fontWeight:700
  },
  typographyPhoto:{
    display:'flex',
    flexDirection:'row',
    columnGap:32,
    fontWeight:700,
    fontSize:16
  },
  admin:{
    display:'flex',
    flexDirection:'row',
    columnGap:12,
    alignItems:'center',
    color: '#202327'
  },
  badge: {
    backgroundColor:' #F1F4F9',
    borderRadius:'50%',
    padding:10,
  },
  appBar:{
    boxShadow:'none',
    borderBottom:'1px solid #E8E8E8',
    marginLeft:'273px',
    width:'calc(100% - 273px)',
    position:'fixed',
    paddingTop: 6,
    paddingBottom:6,
    backgroundColor:'#FFFFFF'
  }
}))

const TopbarComponent = ({children}) => {

  const classes = useStyles()
  return (
    <React.Fragment>
      <AppBar className={classes.appBar}>
          <Toolbar className={classes.toolbar}>
            <Typography component={'span'} className={classes.typography}>
              {children}
            </Typography>

            <Typography component={'span'} className={classes.typographyPhoto}>
              <div className={classes.admin}>
                <span>Shanilda Sabilla</span>
                <img src={photoIcon} alt="" />
              </div>
              {/* <Badge overlap='rectangular' className={classes.badge}></Badge> */}

              <IconButton style={{backgroundColor:'#F1F4F9', boxShadow:'0px 4px 10px rgba(124, 121, 165, 0.15)'}}>  
                <img src={logoutIcon} alt="" width={22} />
              </IconButton>
            </Typography>
         </Toolbar>
      </AppBar>
    </React.Fragment>
  )
}

export default TopbarComponent