import { FormControl, InputLabel, makeStyles } from "@material-ui/core";
import React from "react";

const useStyles = makeStyles((theme) => ({
  inputText: {
    borderRadius: "10px",
    position: "relative",
    backgroundColor: "none",
    width: "100%",
    padding: "9px 7px 9px 12px",
    border: "1px solid #2F82FF",
    fontSize: "14px",
    color: "#8496AE",
    marginTop:'8px',
    "&:focus ":{
        outline:'none',
    }
  },
  label: {
    fonrSize: "16px",
    fontWeight: "500",

  },
}));
const InputFieldComponent = ({label,placeholder}) => {
  const classes = useStyles();
  return (
    <>
      <div >
        <label className={classes.label} htmlFor="username">
          {label} :
        </label>
        <input
          className={classes.inputText}
          placeholder={placeholder}
          name="username"
        />
      </div>
    </>
  );
};

export default InputFieldComponent;
