import React from 'react'
import SidebarComponent from '../components/SidebarComponent'
import TopbarComponent from '../components/TopbarComponent'

const Layout = (props) => {
  return (
   <React.Fragment>
      <TopbarComponent>{props.title}</TopbarComponent>
      <SidebarComponent/>
      <div className='outlet'>
        {props.children}
      </div>
   </React.Fragment>
  )
}

export default Layout