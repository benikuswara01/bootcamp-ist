import React from "react";
import {
  Grid,
  makeStyles,
  Paper,
  Button,
  Typography,
  Box,
  Container,
  Card,
  CardContent,
  FormControl,
  InputLabel,
} from "@material-ui/core";
import Image from "../../assets/image/login-image.svg";
import ImageLogin from "../../assets/image/logo-login.svg";
import InputFieldComponent from "../../components/InputFieldComponent";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(0),
    textAlign: "center",
    color: theme.palette.text.secondary,
    height: "100vh",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  paperImage: {
    backgroundImage: `url(${Image})`,
    backgroundSize: "cover",
  },
  cardContainer: {
    margin: theme.spacing(0),
    backgroundColor: "#F1F4F9",
    height: "100vh",
    padding: theme.spacing(0),
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  card: {
    width: "450px",
    height: "550px",
    padding: "28px",
    borderRadius: "40px",
    boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
  
  },
  buttonText:{
    backgroundColor: "#3A90EF",
    "&:hover": {
      //you want this to be the same as the backgroundColor above
      backgroundColor: "#3A90EF"
  }
    ,
    fontSize: "16px",
    fontWeight: "regular",
    color: "#FFFFFF",
    textTransform: 'none',
  },
  title:{
    fontSize: "40px",
    fontWeight: "500",
    lineHeight: "54px",
  },
  subTitle:{
    fontSize: "16px",
    fontWeight: "medium",
    margin: '16px 0px'
    
  },
  logo:{
    display: 'flex',
    justifyContent: 'center',
    
  }
}));

const LoginPage = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={6}>
          <Paper className={`${classes.paper} ${classes.paperImage}`}></Paper>
        </Grid>
        <Grid item xs={6}>
          <div
            className={`${classes.cardContainer} `}
          >
            <Card className={`${classes.card} `}>
              <CardContent>
                <form>
                  <div  className={classes.logo}>
                    <Box
                      component="img"
                      sx={{
                        height: 120,
                        width: 120,
                        maxHeight: { xs: 233, md: 167 },
                        maxWidth: { xs: 350, md: 250 },
                      }}
                      alt="logo"
                      src={ImageLogin}
                    />
                  </div>
                  <Typography className={classes.title} variant="h4">Login</Typography>
                  <div className={classes.subTitle}>
                    Selamat Datang di
                    <span className="text-[#0597F2]"> EvilCorps</span>
                  </div>
                  <div className="mb-4">
                      <InputFieldComponent label="Username" placeholder="username" />
                  </div>
                  <div className="mb-8">
                      <InputFieldComponent label="Password" placeholder="password" />
                  </div>
                      
                  {/* <TextField
                    id="outlined-basic"
                    label="Email"
                    variant="outlined"
                    fullWidth
                    margin="normal"
                  />

                  <TextField
                    id="outlined-basic"
                    label="Password"
                    variant="outlined"
                    fullWidth
                    margin="normal"
                  /> */}

                  <Button variant="contained" className={classes.buttonText}  fullWidth>
                    Login
                  </Button>
                </form>
              </CardContent>
            </Card>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default LoginPage;
