import React from 'react'
import { Box, Grid, Paper } from '@material-ui/core'
import { TrendingDown, TrendingUp } from '@material-ui/icons'
import PaperGridComponent from '../../components/PaperGridComponent'
import Layout from '../../layouts/Layout'
import TransactionChart from '../../charts/transactionChart/chart'
import UserAgeChart from '../../charts/userAgeChart/chart'
import TableComponent from '../../components/TableComponent'

const DashboardPage = () => {
  return (
    <Layout title={'Dashboard'}>
      <Grid container spacing={2}>
        <PaperGridComponent
          xs={6}
          sm={3}
          title={'Total Income'}
          icon={<TrendingUp className='w-3'/>}
          percentage={'10.0%'}
          total={856}
          desc={'Transaksi'}
        />
        <PaperGridComponent
          xs={6}
          sm={3}
          title={'Total Outcome'}
          icon={<TrendingDown className='w-3'/>}
          percentage={'7.0%'}
          total={17}
          desc={'Transaksi'}
        />
        <PaperGridComponent
          xs={6}
          sm={3}
          title={'Total User'}
          icon={<TrendingUp className='w-3'/>}
          percentage={'12.0%'}
          total={77}
          desc={'User'}
        />
        <PaperGridComponent
          xs={6}
          sm={3}
          title={'Total Transaksi'}
          icon={<TrendingUp className='w-3'/>}
          percentage={'9.0%'}
          total={'3,342'}
          desc={'Transaksi'}
        />
      </Grid>
      
      <Paper className='mt-6 rounded-lg border-2 border-[#ECEEF7]' style={{boxShadow:'none'}}>
        <TransactionChart />
      </Paper>

      <Box className='mt-6 flex row gap-x-8'>
        <TableComponent className='w-3/5' />
        <Paper className='w-2/5 h-max rounded-lg border-2 border-[#ECEEF7]' style={{boxShadow:'none'}}>
          <UserAgeChart/>
        </Paper>
      </Box>
      <div className='pb-5'></div>
    </Layout>
  )
}

export default DashboardPage