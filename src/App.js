import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "./App.css";
import DashboardPage from "./pages/dashboard/DashboardPage";
import LoginPage from "./pages/login/LoginPage";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/login">
          <LoginPage />
        </Route>
        <Route exact path='/dashboard'>
          <DashboardPage/>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
