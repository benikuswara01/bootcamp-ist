import React from 'react'
import 'chart.js/auto';
import { Line } from 'react-chartjs-2'
import { useRef } from 'react';

const TransactionChart = () => {
  const canvasRef = useRef(null)
  const state = {
    labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
    datasets: [
      {
        label: 'Income',
        fill: true,
        tension: 0.3,
        borderColor: '#2F82FF',
        hoverBorderWidth:6,
        pointBackgroundColor:'#2F82FF',
        backgroundColor: ()=>{
          const ctx = canvasRef.current.getContext('2d')
          const gradient1 = ctx.createLinearGradient(200, 0, 200, 480) 
          gradient1.addColorStop(0, 'rgba(47, 130, 255, 0.8)')
          gradient1.addColorStop(0.2, 'rgba(47, 130, 255, 0.34)')
          gradient1.addColorStop(0.4, 'rgba(47, 130, 255, 0.15)')
          gradient1.addColorStop(0.6, 'rgba(47, 130, 255, 0)')

          return gradient1
        },
        data: [63, 66, 32, 55, 32, 58, 41, 18, 64, 101, 57, 59]
      },
      {
        label: 'Outcome',
        fill: true,
        tension: 0.3,
        borderColor: '#4836BF',
        hoverBorderWidth:6,
        pointBackgroundColor:'#4836BF',
        backgroundColor: () => {
          const ctx = canvasRef.current.getContext('2d')
          const gradient2 = ctx.createLinearGradient(150, 10, 150, 500) 
          gradient2.addColorStop(0, 'rgba(72, 54, 191, 0.8)')
          gradient2.addColorStop(0.2, 'rgba(72, 54, 191, 0.34)')
          gradient2.addColorStop(0.4, 'rgba(72, 54, 191, 0.15)')
          gradient2.addColorStop(0.6, 'rgba(72, 54, 191, 0)')

          return gradient2
        },
        data: [56, 92, 84, 8, 39, 36, 21, 43, 39, 22, 17, 101]
      }
    ]
  }

  const options = {
    responsive:true,
    maintainAspectRatio: false,
    layout: { padding: { left:30, right:30, bottom:10, top:6 }},
    plugins: {
      title: {
        display: true,
        text:'Bulan',
        position:'top',
        color:'#323A43',
        font: {
          size:10,
          weight:500
        }
      },
      subtitle: {
        display: true,
        text:'Total Transaksi',
        position:'left',
        color:'#323A43',
        font: { size:10 }
      },
      legend: {
        display: true,
        position:'bottom',
        labels: {
          color: '#000000',
          padding: 20,
          boxWidth:8,
          boxHeight:8,
          usePointStyle:true,
          pointStyle:'circle',
            font: {
              size:14,
              weight:'bold'
            }
        }
      }
    },
    scales:{
      y: { 
        beginAtZero: true, 
        ticks: { 
          color:'#8496AE', 
          font: { size:10 }
        }, grid: { color:'#F0F0F0'}},
      x:{
        ticks:{
          color:'#8496AE',
          font:{ size:10 }
        }, grid: { color:'#F0F0F0' },
      }
    }
  }

  return (
    <React.Fragment>
      <div className='font-medium text-xl flex row justify-between px-7 pt-6'>
        <span>Chart Transaksi</span>
        <select></select>
      </div>
      <div style={{height:360,}}>
        <Line 
          data={state}
          options={options}
        />
        <canvas ref={canvasRef}/>
      </div>
    </React.Fragment>
  )
}
export default TransactionChart