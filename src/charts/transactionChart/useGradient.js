import { useRef } from 'react'

const useGradient = () => {
   const gradientRef = useRef(null)

   const createGradient = canvas => {
      const ctx = canvas.getContext('2d')
   
      // gradient1
      const gradient = ctx.createLinearGradient(200, 0, 200, 480) 
      gradient.addColorStop(0, 'rgba(47, 130, 255, 0.8)')
      gradient.addColorStop(0.2, 'rgba(47, 130, 255, 0.34)')
      gradient.addColorStop(0.4, 'rgba(47, 130, 255, 0.15)')
      gradient.addColorStop(0.6, 'rgba(47, 130, 255, 0)')
   
      // gradient2
      // const gradient2 = ctx.createLinearGradient(150, 10, 150, 500) 
      // gradient2.addColorStop(0, 'rgba(72, 54, 191, 0.8)')
      // gradient2.addColorStop(0.2, 'rgba(72, 54, 191, 0.34)')
      // gradient2.addColorStop(0.4, 'rgba(72, 54, 191, 0.15)')
      // gradient2.addColorStop(0.6, 'rgba(72, 54, 191, 0)')
   
      gradientRef.current = gradient
   }

   return [gradientRef, createGradient]
}

export default useGradient

// in file chart.js
// import useGradient from './useGradient';
// const [gradientRef, createGradient] = useGradient()
// useEffect(() => {
//   createGradient(canvasRef.current)
// }, [])






