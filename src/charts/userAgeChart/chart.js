import React from 'react'
import 'chart.js/auto';
import { Doughnut } from 'react-chartjs-2'

const UserAgeChart = () => {

  const state = {
    labels: [
      'Umur 17 - 30',
      'Umur 30 - 50',
      'Umur 50 Keatas'
    ],
    datasets: [{
      data: [160, 80, 200],
      borderWidth:0,
      hoverBorderWidth:8,
      backgroundColor: [
        '#5932EA',
        '#16C098',
        '#B216C0',
      ],
      borderColor:[
        '#5932EA',
        '#16C098',
        '#B216C0',
      ],
      hoverBorderRadius:0.5
    }]
  }

  const options ={
    responsive:true,
    maintainAspectRatio: false,
    cutout: 55,
    layout: { padding: { left:20, right:20, bottom:0, top:24 }},
    plugins:{
      tooltip:{
        backgroundColor:'rgba(25, 25, 112, 0.8)',
        position:'nearest',
        enabled:false
      },
      legend: {
        display: true,
        position:'right',
        labels: {
          color: '#949494',
          padding:20,
          boxWidth:8,
          boxHeight:8,
          usePointStyle:true,
          pointStyle:'circle',
            font: {
              size:12,
              weight:400
            }
        },
      }
    },
  }
  return (
    <div style={{height:300}}>
      <div className='pt-3 px-5 -mb-10 font-bold text-xl'>
        Umur Pengguna
      </div>
      <Doughnut data={state} options={options}/>
    </div>
  )
}

export default UserAgeChart